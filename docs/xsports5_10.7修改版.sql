/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2023/10/7 15:45:36                           */
/*==============================================================*/


drop table if exists athlete;

drop table if exists event;

drop table if exists game;

drop table if exists groupings;

drop table if exists historyrecord;

drop table if exists member;

drop table if exists notice;

drop table if exists record;

drop table if exists schedule;

drop table if exists signup;

drop table if exists sport;

drop table if exists team;

/*==============================================================*/
/* Table: athlete                                               */
/*==============================================================*/
create table athlete
(
   id                   int not null auto_increment comment '运动员编号',
   o_id                 int comment '单位编号',
   username             varchar(20) comment '用户名',
   realname             varchar(40) comment '姓名',
   birth                date comment '生日',
   gender               char(2) comment '性别',
   mobile               char(11) comment '联系方式',
   email                varchar(64) comment '邮箱',
   password             char(64) comment '密码',
   register_time        timestamp comment '注册时间',
   status               tinyint comment '帐号状态',
   primary key (id)
);

alter table athlete comment '运动员';

/*==============================================================*/
/* Table: event                                                 */
/*==============================================================*/
create table event
(
   id                   int not null auto_increment comment '赛项编号',
   g_id                 int not null comment '运动会编号',
   name                 varchar(40) comment '赛项名称',
   discipline           varchar(20) comment '所属分项',
   sport                varchar(20) comment '所属大项',
   remarks              varchar(200) comment '备注',
   keywords             varchar(200) comment '关键字',
   groupings            varchar(20) comment '组别',
   gender               char(2) comment '性别',
   start_time           timestamp comment '报名开始时间',
   end_time             timestamp comment '报名结束时间',
   status               tinyint comment '状态',
   team                 tinyint comment '是否团体赛，1代表个人，2代表团体',
   primary key (id)
);

alter table event comment '赛项';

/*==============================================================*/
/* Table: game                                                  */
/*==============================================================*/
create table game
(
   id                   int not null auto_increment comment '运动会编号',
   o_id                 int comment '单位编号',
   name                 varchar(40) comment '运动会名称',
   start_time           timestamp comment '运动会开始时间',
   end_time             timestamp comment '运动会结束时间',
   remarks              varchar(255) comment '备注',
   primary key (id)
);

alter table game comment '运动会';

/*==============================================================*/
/* Table: groupings                                             */
/*==============================================================*/
create table groupings
(
   id                   int not null auto_increment comment '组别编号',
   g_id                 int comment '运_运动会编号',
   name                 varchar(64) comment '组名',
   remarks              varchar(255) comment '备注',
   create_time          timestamp comment '创建时间',
   primary key (id)
);

alter table groupings comment '组别';

/*==============================================================*/
/* Table: historyrecord                                         */
/*==============================================================*/
create table historyrecord
(
   id                   int not null auto_increment comment '历史记录编号',
   o_id                 int comment '单位编号',
   athlete_name         varchar(20) comment '运动员姓名',
   sport_name           varchar(64) comment '运动项目名称',
   score                varchar(20) comment '成绩字符串',
   number_score         int comment '成绩数值',
   rule                 varchar(20) comment '排序规则',
   break_time           datetime comment '破记录时间',
   primary key (id)
);

alter table historyrecord comment '历史记录表';

/*==============================================================*/
/* Table: member                                                */
/*==============================================================*/
create table member
(
   t_id                 int not null comment '团队编号',
   a_id                 int not null comment '运动员编号',
   primary key (t_id, a_id)
);

alter table member comment '团体比赛项目团体人员表';

/*==============================================================*/
/* Table: notice                                                */
/*==============================================================*/
create table notice
(
   id                   int not null auto_increment comment '公告编号',
   s_id                 int comment '赛程编号',
   title                varchar(255) comment '标题',
   keywords             varchar(40) comment '关键词',
   content              text comment '文本内容',
   create_time          timestamp comment '发布时间',
   update_time          timestamp comment '修改时间',
   status               tinyint comment '状态',
   primary key (id)
);

alter table notice comment '公告';

/*==============================================================*/
/* Table: record                                                */
/*==============================================================*/
create table record
(
   id                   int not null auto_increment comment '比赛人员成绩记录编号',
   s_id                 int comment '赛程编号',
   score                varchar(64) comment '比赛成绩',
   ranking              smallint comment '比赛名次',
   p_id                 int,
   primary key (id)
);

alter table record comment '比赛人员记录表';

/*==============================================================*/
/* Table: schedule                                              */
/*==============================================================*/
create table schedule
(
   id                   int not null auto_increment comment '赛程表编号',
   g_id                 int comment '运动会编号',
   e_id                 int comment '赛项编号',
   game_time            timestamp comment '比赛时间',
   place                varchar(40) comment '比赛地点',
   remarks              varchar(40) comment '备注',
   status               tinyint comment '状态',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table schedule comment '赛程表';

/*==============================================================*/
/* Table: signup                                                */
/*==============================================================*/
create table signup
(
   id                   int not null auto_increment comment '报名编号',
   e_id                 int comment '赛项编号',
   a_id                 int not null comment '运动员编号',
   signup_time          timestamp comment '报名时间',
   status               tinyint comment '审核状态',
   verify_time          timestamp comment '审核时间',
   primary key (id)
);

alter table signup comment '报名';

/*==============================================================*/
/* Table: sport                                                 */
/*==============================================================*/
create table sport
(
   id                   smallint not null auto_increment comment '运动项目编号',
   name                 varchar(64) comment '项目名称',
   parent               smallint comment '父类编号',
   create_time          timestamp comment '创建时间',
   primary key (id)
);

alter table sport comment '运动项目表';

/*==============================================================*/
/* Table: team                                                  */
/*==============================================================*/
create table team
(
   id                   int not null auto_increment comment '团队编号',
   e_id                 int comment '赛项id',
   size                 tinyint comment '团队人数',
   remarks              varchar(64) comment '团队备注',
   create_time          timestamp comment '创建时间',
   update_time          timestamp comment '修改时间',
   primary key (id)
);

alter table event add constraint fk_r_game_event foreign key (g_id)
      references game (id) on delete restrict on update restrict;

alter table groupings add constraint fk_r_game_grouping foreign key (g_id)
      references game (id) on delete restrict on update restrict;

alter table member add constraint fk_团队人员 foreign key (a_id)
      references athlete (id) on delete restrict on update restrict;

alter table member add constraint fk_团队人员 foreign key (t_id)
      references team (id) on delete restrict on update restrict;

alter table notice add constraint fk_r_schedule_notice foreign key (s_id)
      references schedule (id) on delete restrict on update restrict;

alter table record add constraint fk_r_schedule_record foreign key (s_id)
      references schedule (id) on delete restrict on update restrict;

alter table schedule add constraint fk_r_event_schedule foreign key (e_id)
      references event (id) on delete restrict on update restrict;

alter table schedule add constraint fk_r_game_schedule foreign key (g_id)
      references game (id) on delete restrict on update restrict;

alter table signup add constraint fk_r_athlete_signin foreign key (a_id)
      references athlete (id) on delete restrict on update restrict;

alter table signup add constraint fk_r_event_signup foreign key (e_id)
      references event (id) on delete restrict on update restrict;

alter table team add constraint fk_r_item_team foreign key (e_id)
      references event (id) on delete restrict on update restrict;

