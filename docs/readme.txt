学生端页面需求
1、后台-运动员管理需要将状态修改为 未认证0和已认证1 图标样式
√ 2、学生端-提供学生端登录页面
√ 3、学生端-登录成功后若状态为0 则展示认证页面 完善信息
    否则展示报名页面 进行报名
√4、学生端-认证页面需要完善的信息（生日、性别、邮箱、密码）
5、学生端-报名页面需要的信息（赛项）
6、后台-报名管理需要将状态修改为 未审核0、通过1和未通过2 图标样式
7、后台-报名管理审核图标展示需要优化
8、学生端-展示学生报名情况
9、后台-所有下拉框的宽度需要调整和输入框一致
√ 10、学生端-展示公告信息

学生端功能完善
1、首页直页接展示登录页 目前需要点击返回首页才能跳转
2、报名展示页面 应该展示当前学生的报名项目（目前展示的好像是假数据）
3、报名展示页面 /my 请求参数aid修改为aId
4、报名按钮无法点击 无法跳转后续页面
5、需要报名页面、公告页面、赛程页面

运动员登录接口
get
http://localhost:8080/xsports/athlete/login
?username=13813901054&password=1234
{"msg":"操作成功","code":200,
"data":{"createBy":null,
"createTime":null,"updateBy":null,"updateTime":null,
"remark":null,"id":1,"oId":103,"oName":null,
"username":"13813901054","realname":"王锦堃",
"birth":null,"gender":null,"mobile":"13813901054",
"email":null,"password":"e10adc3949ba59abbe56e057f20f883e",
"registerTime":"2023-08-30","status":0}}
/{"msg":"用户名或密码错误","code":500}

提供运动员信息完善接口
get
http://localhost:8080/xsports/athlete/update
?id=4&birth=2023-8-28&gender=0&email=123@qq.com&password=456
{"msg":"操作成功","code":200}

展示所有赛项接口
get
http://localhost:8080/xsports/event/all
[{"createBy":null,"createTime":null,
"updateBy":null,"updateTime":null,"remark":null,"id":3,
"gId":4,"gName":"淮南运动会","name":"test",
"discipline":"100米","sport":"田径","remarks":"test",
"keywords":"test","groupings":"女子甲组","gender":"0",
"startTime":"2023-08-21","endTime":"2023-08-31","status":null}]

运动员报名
get
http://localhost:8080/xsports/signup/insert
?aId=4&eId=1
{"msg":"操作成功","code":200}

报名展示列表
http://localhost:8080/xsports/signup/my
?aId=5
[{"createBy":null,"createTime":null,"updateBy":null,
"updateTime":null,"remark":null,"aId":6,"aName":"王锦堃2",
"eName":"test","id":2,"eId":3,"signupTime":"2023-08-17",
"status":0,"verifyTime":null}]

公告列表
http://localhost:8080/xsports/notice/all
[{"createBy":null,"createTime":"2023-08-30 16:33:27","updateBy":null,"updateTime":null,"remark":null,"id":1,"sId":1,"sName":"男子100米",
"title":"男子100米决赛成绩已出",
"keywords":"100米决赛",
"content":"<p>测试数据</p>","status":0}]