import request from '@/utils/request'

// 查询公告列表
export function listNotice(query) {
  return request({
    url: '/xsports/notice/list',
    method: 'get',
    params: query
  })
}

//获取所有的赛程
export function allSchedules(){
  return request({
    url: '/xsports/schedule/all',
    method: 'get'
  })
}

// 查询公告详细
export function getNotice(id) {
  return request({
    url: '/xsports/notice/' + id,
    method: 'get'
  })
}

// 新增公告
export function addNotice(data) {
  return request({
    url: '/xsports/notice',
    method: 'post',
    data: data
  })
}

// 修改公告
export function updateNotice(data) {
  return request({
    url: '/xsports/notice',
    method: 'put',
    data: data
  })
}

// 删除公告
export function delNotice(id) {
  return request({
    url: '/xsports/notice/' + id,
    method: 'delete'
  })
}
