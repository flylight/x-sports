import request from '@/utils/request'

// 查询组别列表
export function listGroupings(query) {
  return request({
    url: '/xsports/groupings/list',
    method: 'get',
    params: query
  })
}

//获取所有的运动会
export function allGames(){
  return request({
    url: '/xsports/game/all',
    method: 'get'
  })
}

// 查询组别详细
export function getGroupings(id) {
  return request({
    url: '/xsports/groupings/' + id,
    method: 'get'
  })
}

// 新增组别
export function addGroupings(data) {
  return request({
    url: '/xsports/groupings',
    method: 'post',
    data: data
  })
}

// 修改组别
export function updateGroupings(data) {
  return request({
    url: '/xsports/groupings',
    method: 'put',
    data: data
  })
}

// 删除组别
export function delGroupings(id) {
  return request({
    url: '/xsports/groupings/' + id,
    method: 'delete'
  })
}
