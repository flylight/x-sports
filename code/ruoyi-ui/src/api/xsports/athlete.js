import request from '@/utils/request'

// 查询运动员列表
export function listAthlete(query) {
  return request({
    url: '/xsports/athlete/list',
    method: 'get',
    params: query
  })
}

// 查询运动员详细
export function getAthlete(id) {
  return request({
    url: '/xsports/athlete/' + id,
    method: 'get'
  })
}

// 新增运动员
export function addAthlete(data) {
  return request({
    url: '/xsports/athlete',
    method: 'post',
    data: data
  })
}

// 修改运动员
export function updateAthlete(data) {
  return request({
    url: '/xsports/athlete',
    method: 'put',
    data: data
  })
}

// 删除运动员
export function delAthlete(id) {
  return request({
    url: '/xsports/athlete/' + id,
    method: 'delete'
  })
}
