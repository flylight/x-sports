import request from '@/utils/request'

// 查询运动会列表
export function listGame(query) {
  return request({
    url: '/xsports/game/list',
    method: 'get',
    params: query
  })
}

// 查询运动会详细
export function getGame(id) {
  return request({
    url: '/xsports/game/' + id,
    method: 'get'
  })
}

// 新增运动会
export function addGame(data) {
  return request({
    url: '/xsports/game',
    method: 'post',
    data: data
  })
}

// 修改运动会
export function updateGame(data) {
  return request({
    url: '/xsports/game',
    method: 'put',
    data: data
  })
}

// 删除运动会
export function delGame(id) {
  return request({
    url: '/xsports/game/' + id,
    method: 'delete'
  })
}
