import request from '@/utils/request'

// 查询赛程列表
export function listSchedule(query) {
  return request({
    url: '/xsports/schedule/list',
    method: 'get',
    params: query
  })
}

//获取所有的运动会
export function allGames(){
  return request({
    url: '/xsports/game/all',
    method: 'get'
  })
}

export function allevents(){
  return request({
    url: '/xsports/event/all',
    method: 'get'
  })
}

// 查询赛程详细
export function getSchedule(id) {
  return request({
    url: '/xsports/schedule/' + id,
    method: 'get'
  })
}

// 新增赛程
export function addSchedule(data) {
  return request({
    url: '/xsports/schedule',
    method: 'post',
    data: data
  })
}

// 修改赛程
export function updateSchedule(data) {
  return request({
    url: '/xsports/schedule',
    method: 'put',
    data: data
  })
}

// 删除赛程
export function delSchedule(id) {
  return request({
    url: '/xsports/schedule/' + id,
    method: 'delete'
  })
}
