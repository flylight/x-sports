import request from '@/utils/request'

// 查询比赛人员记录列表
export function listRecord(query) {
  return request({
    url: '/xsports/record/list',
    method: 'get',
    params: query
  })
}

// 查询比赛人员记录详细
export function getRecord(id) {
  return request({
    url: '/xsports/record/' + id,
    method: 'get'
  })
}

// 新增比赛人员记录
export function addRecord(data) {
  return request({
    url: '/xsports/record',
    method: 'post',
    data: data
  })
}

// 修改比赛人员记录
export function updateRecord(data) {
  return request({
    url: '/xsports/record',
    method: 'put',
    data: data
  })
}

// 删除比赛人员记录
export function delRecord(id) {
  return request({
    url: '/xsports/record/' + id,
    method: 'delete'
  })
}
