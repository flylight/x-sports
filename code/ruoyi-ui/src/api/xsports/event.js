import request from '@/utils/request'

// 查询赛项列表
export function listEvent(query) {
  return request({
    url: '/xsports/event/list',
    method: 'get',
    params: query
  })
}

//获取所有的运动会
export function allGames(){
  return request({
    url: '/xsports/game/all',
    method: 'get'
  })
}

//获取所有的组别
export function allGroupings(){
  return request({
    url: '/xsports/groupings/all',
    method: 'get'
  })
}

//获取所有的一级运动项目
export function allParentSport(){
  return request({
    url: '/xsports/sport/allParent',
    method: 'get'
  })
}

//获取所有的分项
export function allSmallSport(pId){
  return request({
    url: '/xsports/sport/allSmall',
    method: 'get',
    params: {pId: pId}
  })
}

// 查询赛项详细
export function getEvent(id) {
  return request({
    url: '/xsports/event/' + id,
    method: 'get'
  })
}

// 新增赛项
export function addEvent(data) {
  return request({
    url: '/xsports/event',
    method: 'post',
    data: data
  })
}

// 修改赛项
export function updateEvent(data) {
  return request({
    url: '/xsports/event',
    method: 'put',
    data: data
  })
}

// 删除赛项
export function delEvent(id) {
  return request({
    url: '/xsports/event/' + id,
    method: 'delete'
  })
}
