import request from '@/utils/request'

// 查询运动项目列表
export function listSport(query) {
  return request({
    url: '/xsports/sport/list',
    method: 'get',
    params: query
  })
}

//获取所有的一级运动项目
export function allParentSport(){
  return request({
    url: '/xsports/sport/allParent',
    method: 'get'
  })
}

// 查询运动项目详细
export function getSport(id) {
  return request({
    url: '/xsports/sport/' + id,
    method: 'get'
  })
}

// 新增运动项目
export function addSport(data) {
  return request({
    url: '/xsports/sport',
    method: 'post',
    data: data
  })
}

// 修改运动项目
export function updateSport(data) {
  return request({
    url: '/xsports/sport',
    method: 'put',
    data: data
  })
}

// 删除运动项目
export function delSport(id) {
  return request({
    url: '/xsports/sport/' + id,
    method: 'delete'
  })
}
