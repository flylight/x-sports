import request from '@/utils/request'

// 查询历史记录列表
export function listHistoryrecord(query) {
  return request({
    url: '/xsports/historyrecord/list',
    method: 'get',
    params: query
  })
}

export function allDepts(){
  return request({
    url: '/system/dept/all',
    method: 'get'
  })
}

// 查询历史记录详细
export function getHistoryrecord(id) {
  return request({
    url: '/xsports/historyrecord/' + id,
    method: 'get'
  })
}

// 新增历史记录
export function addHistoryrecord(data) {
  return request({
    url: '/xsports/historyrecord',
    method: 'post',
    data: data
  })
}

// 修改历史记录
export function updateHistoryrecord(data) {
  return request({
    url: '/xsports/historyrecord',
    method: 'put',
    data: data
  })
}

// 删除历史记录
export function delHistoryrecord(id) {
  return request({
    url: '/xsports/historyrecord/' + id,
    method: 'delete'
  })
}
