import request from '@/utils/request'

// 查询报名列表
export function listSignup(query) {
  return request({
    url: '/xsports/signup/list',
    method: 'get',
    params: query
  })
}

export function allevents(){
  return request({
    url: '/xsports/event/all',
    method: 'get'
  })
}

// 查询报名详细
export function getSignup(aId) {
  return request({
    url: '/xsports/signup/' + aId,
    method: 'get'
  })
}

// 新增报名
export function addSignup(data) {
  return request({
    url: '/xsports/signup',
    method: 'post',
    data: data
  })
}

// 修改报名
export function updateSignup(data) {
  return request({
    url: '/xsports/signup/update',
    method: 'get',
    params: data
  })
}

// 删除报名
export function delSignup(aId) {
  return request({
    url: '/xsports/signup/' + aId,
    method: 'delete'
  })
}
