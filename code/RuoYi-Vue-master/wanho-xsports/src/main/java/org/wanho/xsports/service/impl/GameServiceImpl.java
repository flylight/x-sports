package org.wanho.xsports.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.mapper.GameMapper;
import org.wanho.xsports.domain.Game;
import org.wanho.xsports.service.IGameService;

/**
 * 运动会Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class GameServiceImpl implements IGameService 
{
    @Autowired
    private GameMapper gameMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询运动会
     * 
     * @param id 运动会主键
     * @return 运动会
     */
    @Override
    public Game selectGameById(Long id)
    {
        return gameMapper.selectGameById(id);
    }

    /**
     * 查询运动会列表
     * 
     * @param game 运动会
     * @return 运动会
     */
    @Override
    public List<Game> selectGameList(Game game)
    {
        List<Game> games = gameMapper.selectGameList(game);
        for(Game g : games){
            SysDept sysDept = sysDeptMapper.selectDeptById(g.getoId());
            g.setDeptName(sysDept.getDeptName());
        }
        return games;
    }

    /**
     * 新增运动会
     * 
     * @param game 运动会
     * @return 结果
     */
    @Override
    public int insertGame(Game game)
    {
        //获取登录用户信息
        LoginUser loginUser = SecurityUtils.getLoginUser();
        //设置运动会所属单位编号
        game.setoId(loginUser.getDeptId());
        return gameMapper.insertGame(game);
    }

    /**
     * 修改运动会
     * 
     * @param game 运动会
     * @return 结果
     */
    @Override
    public int updateGame(Game game)
    {
        return gameMapper.updateGame(game);
    }

    /**
     * 批量删除运动会
     * 
     * @param ids 需要删除的运动会主键
     * @return 结果
     */
    @Override
    public int deleteGameByIds(Long[] ids)
    {
        return gameMapper.deleteGameByIds(ids);
    }

    /**
     * 删除运动会信息
     * 
     * @param id 运动会主键
     * @return 结果
     */
    @Override
    public int deleteGameById(Long id)
    {
        return gameMapper.deleteGameById(id);
    }
}
