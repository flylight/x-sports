package org.wanho.xsports.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.mapper.SportMapper;
import org.wanho.xsports.domain.Sport;
import org.wanho.xsports.service.ISportService;

/**
 * 运动项目Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-25
 */
@Service
public class SportServiceImpl implements ISportService 
{
    @Autowired
    private SportMapper sportMapper;

    /**
     * 查询运动项目
     * 
     * @param id 运动项目主键
     * @return 运动项目
     */
    @Override
    public Sport selectSportById(Integer id)
    {
        return sportMapper.selectSportById(id);
    }

    /**
     * 查询运动项目列表
     * 
     * @param sport 运动项目
     * @return 运动项目
     */
    @Override
    public List<Sport> selectSportList(Sport sport)
    {
        List<Sport> sports = sportMapper.selectSportList(sport);
        for(Sport s : sports){
            Sport sp = sportMapper.selectSportById(s.getParent());
            s.setParentName(sp == null ? "———" : sp.getName());
        }
        return sports;
    }

    /**
     * 新增运动项目
     * 
     * @param sport 运动项目
     * @return 结果
     */
    @Override
    public int insertSport(Sport sport)
    {
        sport.setCreateTime(DateUtils.getNowDate());
        return sportMapper.insertSport(sport);
    }

    /**
     * 修改运动项目
     * 
     * @param sport 运动项目
     * @return 结果
     */
    @Override
    public int updateSport(Sport sport)
    {
        return sportMapper.updateSport(sport);
    }

    /**
     * 批量删除运动项目
     * 
     * @param ids 需要删除的运动项目主键
     * @return 结果
     */
    @Override
    public int deleteSportByIds(Integer[] ids)
    {
        return sportMapper.deleteSportByIds(ids);
    }

    /**
     * 删除运动项目信息
     * 
     * @param id 运动项目主键
     * @return 结果
     */
    @Override
    public int deleteSportById(Integer id)
    {
        return sportMapper.deleteSportById(id);
    }

    @Override
    public List<Sport> selectParent() {
        return sportMapper.selectParent();
    }
}
