package org.wanho.xsports.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.domain.Schedule;
import org.wanho.xsports.mapper.EventMapper;
import org.wanho.xsports.mapper.NoticeMapper;
import org.wanho.xsports.domain.Notice;
import org.wanho.xsports.mapper.ScheduleMapper;
import org.wanho.xsports.service.INoticeService;

/**
 * 公告Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class NoticeServiceImpl implements INoticeService 
{
    @Autowired
    private NoticeMapper noticeMapper;

    @Autowired
    private ScheduleMapper scheduleMapper;

    @Autowired
    private EventMapper eventMapper;

    /**
     * 查询公告
     * 
     * @param id 公告主键
     * @return 公告
     */
    @Override
    public Notice selectNoticeById(Long id)
    {
        return noticeMapper.selectNoticeById(id);
    }

    /**
     * 查询公告列表
     * 
     * @param notice 公告
     * @return 公告
     */
    @Override
    public List<Notice> selectNoticeList(Notice notice)
    {
        List<Notice> notices = noticeMapper.selectNoticeList(notice);
        for(Notice n : notices){
            Schedule schedule = scheduleMapper.selectScheduleById(n.getsId());
            n.setsName(eventMapper.selectEventById(schedule.geteId()).getName());
        }
        return notices;
    }

    /**
     * 新增公告
     * 
     * @param notice 公告
     * @return 结果
     */
    @Override
    public int insertNotice(Notice notice)
    {
        notice.setCreateTime(DateUtils.getNowDate());
        notice.setStatus(0);
        return noticeMapper.insertNotice(notice);
    }

    /**
     * 修改公告
     * 
     * @param notice 公告
     * @return 结果
     */
    @Override
    public int updateNotice(Notice notice)
    {
        notice.setUpdateTime(DateUtils.getNowDate());
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 批量删除公告
     * 
     * @param ids 需要删除的公告主键
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(Long[] ids)
    {
        return noticeMapper.deleteNoticeByIds(ids);
    }

    /**
     * 删除公告信息
     * 
     * @param id 公告主键
     * @return 结果
     */
    @Override
    public int deleteNoticeById(Long id)
    {
        return noticeMapper.deleteNoticeById(id);
    }
}
