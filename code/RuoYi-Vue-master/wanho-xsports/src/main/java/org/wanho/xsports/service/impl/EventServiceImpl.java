package org.wanho.xsports.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.domain.Signup;
import org.wanho.xsports.domain.Sport;
import org.wanho.xsports.mapper.EventMapper;
import org.wanho.xsports.domain.Event;
import org.wanho.xsports.mapper.GameMapper;
import org.wanho.xsports.mapper.SignupMapper;
import org.wanho.xsports.mapper.SportMapper;
import org.wanho.xsports.service.IEventService;

/**
 * 赛项Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class EventServiceImpl implements IEventService 
{
    @Autowired
    private EventMapper eventMapper;

    @Autowired
    private SportMapper sportMapper;

    @Autowired
    private GameMapper gameMapper;

    @Autowired
    private SignupMapper signupMapper;

    /**
     * 查询赛项
     * 
     * @param id 赛项主键
     * @return 赛项
     */
    @Override
    public Event selectEventById(Long id)
    {
        return eventMapper.selectEventById(id);
    }

    /**
     * 查询赛项列表
     * 
     * @param event 赛项
     * @return 赛项
     */
    @Override
    public List<Event> selectEventList(Event event)
    {
        List<Event> events = eventMapper.selectEventList(event);
        for (Event e : events){
            e.setgName(gameMapper.selectGameById(e.getgId()).getName());
            e.setValue(e.getId());
            //查询当前赛项的审核通过报名人数
            Signup signup = new Signup();
            signup.setStatus(0);
            signup.seteId(e.getId());
            Integer count = signupMapper.selectCount(signup);
            e.setNum(count);
        }
        return events;
    }

    /**
     * 新增赛项
     * 
     * @param event 赛项
     * @return 结果
     */
    @Override
    public int insertEvent(Event event)
    {
        Sport sport = sportMapper.selectSportById(Integer.parseInt(event.getSport()));
        Sport disc = sportMapper.selectSportById(Integer.parseInt(event.getDiscipline()));
        event.setSport(sport.getName());
        event.setDiscipline(disc.getName());
        return eventMapper.insertEvent(event);
    }

    /**
     * 修改赛项
     * 
     * @param event 赛项
     * @return 结果
     */
    @Override
    public int updateEvent(Event event)
    {
        return eventMapper.updateEvent(event);
    }

    /**
     * 批量删除赛项
     * 
     * @param ids 需要删除的赛项主键
     * @return 结果
     */
    @Override
    public int deleteEventByIds(Long[] ids)
    {
        return eventMapper.deleteEventByIds(ids);
    }

    /**
     * 删除赛项信息
     * 
     * @param id 赛项主键
     * @return 结果
     */
    @Override
    public int deleteEventById(Long id)
    {
        return eventMapper.deleteEventById(id);
    }
}
