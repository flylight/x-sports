package org.wanho.xsports.mapper;

import java.util.List;
import org.wanho.xsports.domain.Signup;

/**
 * 报名Mapper接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface SignupMapper 
{
    /**
     * 查询报名
     * 
     * @param aId 报名主键
     * @return 报名
     */
    public Signup selectSignupByAId(Long aId);

    /**
     * 查询报名列表
     * 
     * @param signup 报名
     * @return 报名集合
     */
    public List<Signup> selectSignupList(Signup signup);

    /*
     * 获取报名人数
     */
    public Integer selectCount(Signup signup);



    /**
     * 新增报名
     * 
     * @param signup 报名
     * @return 结果
     */
    public int insertSignup(Signup signup);

    /**
     * 修改报名
     * 
     * @param signup 报名
     * @return 结果
     */
    public int updateSignup(Signup signup);

    /**
     * 删除报名
     * 
     * @param aId 报名主键
     * @return 结果
     */
    public int deleteSignupByAId(Long aId);

    /**
     * 批量删除报名
     * 
     * @param aIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSignupByAIds(Long[] aIds);
}
