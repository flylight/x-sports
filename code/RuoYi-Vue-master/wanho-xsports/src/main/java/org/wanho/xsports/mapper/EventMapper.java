package org.wanho.xsports.mapper;

import java.util.List;
import org.wanho.xsports.domain.Event;

/**
 * 赛项Mapper接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface EventMapper 
{
    /**
     * 查询赛项
     * 
     * @param id 赛项主键
     * @return 赛项
     */
    public Event selectEventById(Long id);

    /**
     * 查询赛项列表
     * 
     * @param event 赛项
     * @return 赛项集合
     */
    public List<Event> selectEventList(Event event);

    /**
     * 新增赛项
     * 
     * @param event 赛项
     * @return 结果
     */
    public int insertEvent(Event event);

    /**
     * 修改赛项
     * 
     * @param event 赛项
     * @return 结果
     */
    public int updateEvent(Event event);

    /**
     * 删除赛项
     * 
     * @param id 赛项主键
     * @return 结果
     */
    public int deleteEventById(Long id);

    /**
     * 批量删除赛项
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEventByIds(Long[] ids);
}
