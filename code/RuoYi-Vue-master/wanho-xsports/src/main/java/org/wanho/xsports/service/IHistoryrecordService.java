package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Historyrecord;

/**
 * 历史记录Service接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface IHistoryrecordService 
{
    /**
     * 查询历史记录
     * 
     * @param id 历史记录主键
     * @return 历史记录
     */
    public Historyrecord selectHistoryrecordById(Long id);

    /**
     * 查询历史记录列表
     * 
     * @param historyrecord 历史记录
     * @return 历史记录集合
     */
    public List<Historyrecord> selectHistoryrecordList(Historyrecord historyrecord);

    /**
     * 新增历史记录
     * 
     * @param historyrecord 历史记录
     * @return 结果
     */
    public int insertHistoryrecord(Historyrecord historyrecord);

    /**
     * 修改历史记录
     * 
     * @param historyrecord 历史记录
     * @return 结果
     */
    public int updateHistoryrecord(Historyrecord historyrecord);

    /**
     * 批量删除历史记录
     * 
     * @param ids 需要删除的历史记录主键集合
     * @return 结果
     */
    public int deleteHistoryrecordByIds(Long[] ids);

    /**
     * 删除历史记录信息
     * 
     * @param id 历史记录主键
     * @return 结果
     */
    public int deleteHistoryrecordById(Long id);
}
