package org.wanho.xsports.mapper;

import java.util.List;
import org.wanho.xsports.domain.Historyrecord;

/**
 * 历史记录Mapper接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface HistoryrecordMapper 
{
    /**
     * 查询历史记录
     * 
     * @param id 历史记录主键
     * @return 历史记录
     */
    public Historyrecord selectHistoryrecordById(Long id);

    /**
     * 查询历史记录列表
     * 
     * @param historyrecord 历史记录
     * @return 历史记录集合
     */
    public List<Historyrecord> selectHistoryrecordList(Historyrecord historyrecord);

    /**
     * 新增历史记录
     * 
     * @param historyrecord 历史记录
     * @return 结果
     */
    public int insertHistoryrecord(Historyrecord historyrecord);

    /**
     * 修改历史记录
     * 
     * @param historyrecord 历史记录
     * @return 结果
     */
    public int updateHistoryrecord(Historyrecord historyrecord);

    /**
     * 删除历史记录
     * 
     * @param id 历史记录主键
     * @return 结果
     */
    public int deleteHistoryrecordById(Long id);

    /**
     * 批量删除历史记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHistoryrecordByIds(Long[] ids);
}
