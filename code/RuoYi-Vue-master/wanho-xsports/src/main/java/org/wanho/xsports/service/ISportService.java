package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Sport;

/**
 * 运动项目Service接口
 * 
 * @author wanho
 * @date 2023-08-25
 */
public interface ISportService 
{
    /**
     * 查询运动项目
     * 
     * @param id 运动项目主键
     * @return 运动项目
     */
    public Sport selectSportById(Integer id);

    /**
     * 查询运动项目列表
     * 
     * @param sport 运动项目
     * @return 运动项目集合
     */
    public List<Sport> selectSportList(Sport sport);

    /**
     * 新增运动项目
     * 
     * @param sport 运动项目
     * @return 结果
     */
    public int insertSport(Sport sport);

    /**
     * 修改运动项目
     * 
     * @param sport 运动项目
     * @return 结果
     */
    public int updateSport(Sport sport);

    /**
     * 批量删除运动项目
     * 
     * @param ids 需要删除的运动项目主键集合
     * @return 结果
     */
    public int deleteSportByIds(Integer[] ids);

    /**
     * 删除运动项目信息
     * 
     * @param id 运动项目主键
     * @return 结果
     */
    public int deleteSportById(Integer id);

    /*
     * 查询所有的一级运动项目
     */
    public List<Sport> selectParent();
}
