package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Signup;

/**
 * 报名Service接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface ISignupService 
{
    /**
     * 查询报名
     * 
     * @param aId 报名主键
     * @return 报名
     */
    public Signup selectSignupByAId(Long aId);

    /**
     * 查询报名列表
     * 
     * @param signup 报名
     * @return 报名集合
     */
    public List<Signup> selectSignupList(Signup signup);

    /**
     * 新增报名
     * 
     * @param signup 报名
     * @return 结果
     */
    public int insertSignup(Signup signup);

    /**
     * 修改报名
     * 
     * @param signup 报名
     * @return 结果
     */
    public int updateSignup(Signup signup);

    /**
     * 批量删除报名
     * 
     * @param aIds 需要删除的报名主键集合
     * @return 结果
     */
    public int deleteSignupByAIds(Long[] aIds);

    /**
     * 删除报名信息
     * 
     * @param aId 报名主键
     * @return 结果
     */
    public int deleteSignupByAId(Long aId);
}
