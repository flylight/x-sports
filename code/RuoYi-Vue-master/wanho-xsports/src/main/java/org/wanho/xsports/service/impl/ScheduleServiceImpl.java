package org.wanho.xsports.service.impl;

import java.util.ArrayList;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.domain.Athlete;
import org.wanho.xsports.domain.Signup;
import org.wanho.xsports.mapper.*;
import org.wanho.xsports.domain.Schedule;
import org.wanho.xsports.service.IScheduleService;

/**
 * 赛程Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class ScheduleServiceImpl implements IScheduleService 
{
    @Autowired
    private ScheduleMapper scheduleMapper;

    @Autowired
    private GameMapper gameMapper;

    @Autowired
    private EventMapper eventMapper;

    @Autowired
    private AthleteMapper athleteMapper;

    @Autowired
    private SignupMapper signupMapper;

    /**
     * 查询赛程
     * 
     * @param id 赛程主键
     * @return 赛程
     */
    @Override
    public Schedule selectScheduleById(Long id)
    {
        return scheduleMapper.selectScheduleById(id);
    }

    /**
     * 查询赛程列表
     *
     * @param schedule 赛程
     * @return 赛程
     */
    @Override
    public List<Schedule> selectScheduleList(Schedule schedule)
    {
        List<Schedule> schedules = scheduleMapper.selectScheduleList(schedule);
        for(Schedule s : schedules){
            Signup signup = new Signup();
            signup.setStatus(1);
            signup.seteId(s.geteId());
            //获取报名当前赛项且审核通过的运动员信息
            List<Signup> signups = signupMapper.selectSignupList(signup);
            List<Athlete> athletes = new ArrayList<>();
            for(Signup si : signups){
                Athlete athlete = athleteMapper.selectAthleteById(si.getaId());
                athletes.add(athlete);
            }
            s.setgName(gameMapper.selectGameById(s.getgId()).getName());
            s.seteName(eventMapper.selectEventById(s.geteId()).getName());
            s.setAthletes(athletes);
        }
        return schedules;
    }

    /**
     * 新增赛程
     * 
     * @param schedule 赛程
     * @return 结果
     */
    @Override
    public int insertSchedule(Schedule schedule)
    {
        schedule.setStatus(0);
        schedule.setCreateTime(DateUtils.getNowDate());
        return scheduleMapper.insertSchedule(schedule);
    }

    /**
     * 修改赛程
     * 
     * @param schedule 赛程
     * @return 结果
     */
    @Override
    public int updateSchedule(Schedule schedule)
    {
        schedule.setUpdateTime(DateUtils.getNowDate());
        return scheduleMapper.updateSchedule(schedule);
    }

    /**
     * 批量删除赛程
     * 
     * @param ids 需要删除的赛程主键
     * @return 结果
     */
    @Override
    public int deleteScheduleByIds(Long[] ids)
    {
        return scheduleMapper.deleteScheduleByIds(ids);
    }

    /**
     * 删除赛程信息
     * 
     * @param id 赛程主键
     * @return 结果
     */
    @Override
    public int deleteScheduleById(Long id)
    {
        return scheduleMapper.deleteScheduleById(id);
    }
}
