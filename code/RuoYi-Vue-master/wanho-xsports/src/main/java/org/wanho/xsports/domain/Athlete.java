package org.wanho.xsports.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 运动员对象 athlete
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Athlete extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 运动员编号 */
    private Long id;

    /** 单位编号 */
//    @Excel(name = "单位编号")
    private Long oId;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String oName;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 姓名 */
    @Excel(name = "姓名")
    private String realname;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birth;

    /** 性别 */
    @Excel(name = "性别")
    private String gender;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String mobile;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 密码 */
//    @Excel(name = "密码")
    private String password;

    /** 注册时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注册时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registerTime;

    /** 帐号状态 */
    @Excel(name = "帐号状态")
    private Integer status;

    public String getoName() {
        return oName;
    }

    public void setoName(String oName) {
        this.oName = oName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setoId(Long oId) 
    {
        this.oId = oId;
    }

    public Long getoId() 
    {
        return oId;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setRealname(String realname) 
    {
        this.realname = realname;
    }

    public String getRealname() 
    {
        return realname;
    }
    public void setBirth(Date birth) 
    {
        this.birth = birth;
    }

    public Date getBirth() 
    {
        return birth;
    }
    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getGender() 
    {
        return gender;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setRegisterTime(Date registerTime) 
    {
        this.registerTime = registerTime;
    }

    public Date getRegisterTime() 
    {
        return registerTime;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("oId", getoId())
            .append("username", getUsername())
            .append("realname", getRealname())
            .append("birth", getBirth())
            .append("gender", getGender())
            .append("mobile", getMobile())
            .append("email", getEmail())
            .append("password", getPassword())
            .append("registerTime", getRegisterTime())
            .append("status", getStatus())
            .toString();
    }
}
