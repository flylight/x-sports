package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Schedule;

/**
 * 赛程Service接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface IScheduleService 
{
    /**
     * 查询赛程
     * 
     * @param id 赛程主键
     * @return 赛程
     */
    public Schedule selectScheduleById(Long id);

    /**
     * 查询赛程列表
     * 
     * @param schedule 赛程
     * @return 赛程集合
     */
    public List<Schedule> selectScheduleList(Schedule schedule);

    /**
     * 新增赛程
     * 
     * @param schedule 赛程
     * @return 结果
     */
    public int insertSchedule(Schedule schedule);

    /**
     * 修改赛程
     * 
     * @param schedule 赛程
     * @return 结果
     */
    public int updateSchedule(Schedule schedule);

    /**
     * 批量删除赛程
     * 
     * @param ids 需要删除的赛程主键集合
     * @return 结果
     */
    public int deleteScheduleByIds(Long[] ids);

    /**
     * 删除赛程信息
     * 
     * @param id 赛程主键
     * @return 结果
     */
    public int deleteScheduleById(Long id);
}
