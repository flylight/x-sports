package org.wanho.xsports.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.mapper.RecordMapper;
import org.wanho.xsports.domain.Record;
import org.wanho.xsports.service.IRecordService;

/**
 * 比赛人员记录Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class RecordServiceImpl implements IRecordService 
{
    @Autowired
    private RecordMapper recordMapper;

    /**
     * 查询比赛人员记录
     * 
     * @param id 比赛人员记录主键
     * @return 比赛人员记录
     */
    @Override
    public Record selectRecordById(Long id)
    {
        return recordMapper.selectRecordById(id);
    }

    /**
     * 查询比赛人员记录列表
     * 
     * @param record 比赛人员记录
     * @return 比赛人员记录
     */
    @Override
    public List<Record> selectRecordList(Record record)
    {
        return recordMapper.selectRecordList(record);
    }

    /**
     * 新增比赛人员记录
     * 
     * @param record 比赛人员记录
     * @return 结果
     */
    @Override
    public int insertRecord(Record record)
    {
        return recordMapper.insertRecord(record);
    }

    /**
     * 修改比赛人员记录
     * 
     * @param record 比赛人员记录
     * @return 结果
     */
    @Override
    public int updateRecord(Record record)
    {
        return recordMapper.updateRecord(record);
    }

    /**
     * 批量删除比赛人员记录
     * 
     * @param ids 需要删除的比赛人员记录主键
     * @return 结果
     */
    @Override
    public int deleteRecordByIds(Long[] ids)
    {
        return recordMapper.deleteRecordByIds(ids);
    }

    /**
     * 删除比赛人员记录信息
     * 
     * @param id 比赛人员记录主键
     * @return 结果
     */
    @Override
    public int deleteRecordById(Long id)
    {
        return recordMapper.deleteRecordById(id);
    }
}
