package org.wanho.xsports.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.domain.Game;
import org.wanho.xsports.mapper.GameMapper;
import org.wanho.xsports.mapper.GroupingsMapper;
import org.wanho.xsports.domain.Groupings;
import org.wanho.xsports.service.IGroupingsService;

/**
 * 组别Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class GroupingsServiceImpl implements IGroupingsService 
{
    @Autowired
    private GroupingsMapper groupingsMapper;

    @Autowired
    private GameMapper gameMapper;

    /**
     * 查询组别
     * 
     * @param id 组别主键
     * @return 组别
     */
    @Override
    public Groupings selectGroupingsById(Long id)
    {
        return groupingsMapper.selectGroupingsById(id);
    }

    /**
     * 查询组别列表
     * 
     * @param groupings 组别
     * @return 组别
     */
    @Override
    public List<Groupings> selectGroupingsList(Groupings groupings)
    {
//        帅选运动会 只查询当前登陆账号所属部门的运动会
//        Long deptId = SecurityUtils.getLoginUser().getDeptId();
//        Game game = new Game();
//        game.setoId(deptId);
//        List<Game> games = gameMapper.selectGameList(game);

        List<Groupings> groups = groupingsMapper.selectGroupingsList(groupings);
        for(Groupings g : groups){
            Game game = gameMapper.selectGameById(g.getgId());
            g.setgName(game.getName());
        }
        return groups;
    }

    /**
     * 新增组别
     * 
     * @param groupings 组别
     * @return 结果
     */
    @Override
    public int insertGroupings(Groupings groupings)
    {
        groupings.setCreateTime(DateUtils.getNowDate());
        return groupingsMapper.insertGroupings(groupings);
    }

    /**
     * 修改组别
     * 
     * @param groupings 组别
     * @return 结果
     */
    @Override
    public int updateGroupings(Groupings groupings)
    {
        return groupingsMapper.updateGroupings(groupings);
    }

    /**
     * 批量删除组别
     * 
     * @param ids 需要删除的组别主键
     * @return 结果
     */
    @Override
    public int deleteGroupingsByIds(Long[] ids)
    {
        return groupingsMapper.deleteGroupingsByIds(ids);
    }

    /**
     * 删除组别信息
     * 
     * @param id 组别主键
     * @return 结果
     */
    @Override
    public int deleteGroupingsById(Long id)
    {
        return groupingsMapper.deleteGroupingsById(id);
    }
}
