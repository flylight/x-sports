package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Record;

/**
 * 比赛人员记录Service接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface IRecordService 
{
    /**
     * 查询比赛人员记录
     * 
     * @param id 比赛人员记录主键
     * @return 比赛人员记录
     */
    public Record selectRecordById(Long id);

    /**
     * 查询比赛人员记录列表
     * 
     * @param record 比赛人员记录
     * @return 比赛人员记录集合
     */
    public List<Record> selectRecordList(Record record);

    /**
     * 新增比赛人员记录
     * 
     * @param record 比赛人员记录
     * @return 结果
     */
    public int insertRecord(Record record);

    /**
     * 修改比赛人员记录
     * 
     * @param record 比赛人员记录
     * @return 结果
     */
    public int updateRecord(Record record);

    /**
     * 批量删除比赛人员记录
     * 
     * @param ids 需要删除的比赛人员记录主键集合
     * @return 结果
     */
    public int deleteRecordByIds(Long[] ids);

    /**
     * 删除比赛人员记录信息
     * 
     * @param id 比赛人员记录主键
     * @return 结果
     */
    public int deleteRecordById(Long id);
}
