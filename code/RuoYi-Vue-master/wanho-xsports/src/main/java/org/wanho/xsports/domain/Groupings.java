package org.wanho.xsports.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 组别对象 groupings
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Groupings extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 组别编号 */
    private Long id;

    /** 运_运动会编号 */
    @Excel(name = "运_运动会编号")
    private Long gId;

    private String gName;

    /** 组名 */
    @Excel(name = "组名")
    private String name;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setgId(Long gId) 
    {
        this.gId = gId;
    }

    public Long getgId() 
    {
        return gId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gId", getgId())
            .append("name", getName())
            .append("remarks", getRemarks())
            .append("createTime", getCreateTime())
            .toString();
    }
}
