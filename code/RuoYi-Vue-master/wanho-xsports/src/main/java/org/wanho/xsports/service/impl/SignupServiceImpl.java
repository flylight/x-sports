package org.wanho.xsports.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.domain.Athlete;
import org.wanho.xsports.domain.Event;
import org.wanho.xsports.mapper.AthleteMapper;
import org.wanho.xsports.mapper.EventMapper;
import org.wanho.xsports.mapper.SignupMapper;
import org.wanho.xsports.domain.Signup;
import org.wanho.xsports.service.ISignupService;

/**
 * 报名Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class SignupServiceImpl implements ISignupService 
{
    @Autowired
    private SignupMapper signupMapper;

    @Autowired
    private AthleteMapper athleteMapper;

    @Autowired
    private EventMapper eventMapper;

    /**
     * 查询报名
     * 
     * @param aId 报名主键
     * @return 报名
     */
    @Override
    public Signup selectSignupByAId(Long aId)
    {
        return signupMapper.selectSignupByAId(aId);
    }

    /**
     * 查询报名列表
     * 
     * @param signup 报名
     * @return 报名
     */
    @Override
    public List<Signup> selectSignupList(Signup signup)
    {
        List<Signup> signups = signupMapper.selectSignupList(signup);
        for(Signup s : signups){
            Athlete athlete = athleteMapper.selectAthleteById(s.getaId());
            Event event = eventMapper.selectEventById(s.geteId());
            s.setaName(athlete.getRealname());
            s.seteName(event.getName());
        }
        return signups;
    }

    /**
     * 新增报名
     * 
     * @param signup 报名
     * @return 结果
     */
    @Override
    public int insertSignup(Signup signup)
    {
        signup.setSignupTime(new Date());
        signup.setStatus(0);
        signup.setVerifyTime(new Date());
        return signupMapper.insertSignup(signup);
    }

    /**
     * 修改报名
     * 
     * @param signup 报名
     * @return 结果
     */
    @Override
    public int updateSignup(Signup signup)
    {
        return signupMapper.updateSignup(signup);
    }

    /**
     * 批量删除报名
     * 
     * @param aIds 需要删除的报名主键
     * @return 结果
     */
    @Override
    public int deleteSignupByAIds(Long[] aIds)
    {
        return signupMapper.deleteSignupByAIds(aIds);
    }

    /**
     * 删除报名信息
     * 
     * @param aId 报名主键
     * @return 结果
     */
    @Override
    public int deleteSignupByAId(Long aId)
    {
        return signupMapper.deleteSignupByAId(aId);
    }
}
