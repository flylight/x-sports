package org.wanho.xsports.mapper;

import java.util.List;
import org.wanho.xsports.domain.Groupings;

/**
 * 组别Mapper接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface GroupingsMapper 
{
    /**
     * 查询组别
     * 
     * @param id 组别主键
     * @return 组别
     */
    public Groupings selectGroupingsById(Long id);

    /**
     * 查询组别列表
     * 
     * @param groupings 组别
     * @return 组别集合
     */
    public List<Groupings> selectGroupingsList(Groupings groupings);

    /**
     * 新增组别
     * 
     * @param groupings 组别
     * @return 结果
     */
    public int insertGroupings(Groupings groupings);

    /**
     * 修改组别
     * 
     * @param groupings 组别
     * @return 结果
     */
    public int updateGroupings(Groupings groupings);

    /**
     * 删除组别
     * 
     * @param id 组别主键
     * @return 结果
     */
    public int deleteGroupingsById(Long id);

    /**
     * 批量删除组别
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGroupingsByIds(Long[] ids);
}
