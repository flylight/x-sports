package org.wanho.xsports.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 比赛人员记录对象 record
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Record extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 比赛人员成绩记录编号 */
    private Long id;

    /** 赛程编号 */
    @Excel(name = "赛程编号")
    private Long sId;

    /** 运动员编号 */
    @Excel(name = "运动员编号")
    private Long aId;

    /** 比赛成绩 */
    @Excel(name = "比赛成绩")
    private String score;

    /** 比赛名次 */
    @Excel(name = "比赛名次")
    private Integer ranking;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setsId(Long sId) 
    {
        this.sId = sId;
    }

    public Long getsId() 
    {
        return sId;
    }
    public void setaId(Long aId) 
    {
        this.aId = aId;
    }

    public Long getaId() 
    {
        return aId;
    }
    public void setScore(String score) 
    {
        this.score = score;
    }

    public String getScore() 
    {
        return score;
    }
    public void setRanking(Integer ranking) 
    {
        this.ranking = ranking;
    }

    public Integer getRanking() 
    {
        return ranking;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sId", getsId())
            .append("aId", getaId())
            .append("score", getScore())
            .append("ranking", getRanking())
            .toString();
    }
}
