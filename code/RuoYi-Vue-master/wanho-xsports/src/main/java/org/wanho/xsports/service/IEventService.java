package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Event;

/**
 * 赛项Service接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface IEventService 
{
    /**
     * 查询赛项
     * 
     * @param id 赛项主键
     * @return 赛项
     */
    public Event selectEventById(Long id);

    /**
     * 查询赛项列表
     * 
     * @param event 赛项
     * @return 赛项集合
     */
    public List<Event> selectEventList(Event event);

    /**
     * 新增赛项
     * 
     * @param event 赛项
     * @return 结果
     */
    public int insertEvent(Event event);

    /**
     * 修改赛项
     * 
     * @param event 赛项
     * @return 结果
     */
    public int updateEvent(Event event);

    /**
     * 批量删除赛项
     * 
     * @param ids 需要删除的赛项主键集合
     * @return 结果
     */
    public int deleteEventByIds(Long[] ids);

    /**
     * 删除赛项信息
     * 
     * @param id 赛项主键
     * @return 结果
     */
    public int deleteEventById(Long id);
}
