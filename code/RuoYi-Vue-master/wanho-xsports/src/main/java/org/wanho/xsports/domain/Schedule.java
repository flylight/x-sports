package org.wanho.xsports.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 赛程对象 schedule
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Schedule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 赛程表编号 */
    private Long id;

    /** 运动会编号 */
    @Excel(name = "运动会编号")
    private Long gId;

    private String gName;

    /** 赛项编号 */
    @Excel(name = "赛项编号")
    private Long eId;

    private String eName;

    /** 比赛时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "比赛时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gameTime;

    /** 比赛地点 */
    @Excel(name = "比赛地点")
    private String place;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 运动员 */
    private List<Athlete> athletes;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public List<Athlete> getAthletes() {
        return athletes;
    }

    public void setAthletes(List<Athlete> athletes) {
        this.athletes = athletes;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setgId(Long gId) 
    {
        this.gId = gId;
    }

    public Long getgId() 
    {
        return gId;
    }
    public void seteId(Long eId) 
    {
        this.eId = eId;
    }

    public Long geteId() 
    {
        return eId;
    }
    public void setGameTime(Date gameTime) 
    {
        this.gameTime = gameTime;
    }

    public Date getGameTime() 
    {
        return gameTime;
    }
    public void setPlace(String place) 
    {
        this.place = place;
    }

    public String getPlace() 
    {
        return place;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gId", getgId())
            .append("eId", geteId())
            .append("gameTime", getGameTime())
            .append("place", getPlace())
            .append("remarks", getRemarks())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
