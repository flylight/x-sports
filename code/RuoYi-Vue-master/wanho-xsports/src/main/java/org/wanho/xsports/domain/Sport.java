package org.wanho.xsports.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 运动项目对象 sport
 * 
 * @author wanho
 * @date 2023-08-25
 */
public class Sport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 运动项目编号 */
    private Integer id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String name;

    /** 父类编号 */
    @Excel(name = "父类编号")
    private Integer parent;

    private String parentName;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setParent(Integer parent) 
    {
        this.parent = parent;
    }

    public Integer getParent() 
    {
        return parent;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("parent", getParent())
            .append("createTime", getCreateTime())
            .toString();
    }
}
