package org.wanho.xsports.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 历史记录对象 historyrecord
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Historyrecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 历史记录编号 */
    private Long id;

    /** 单位编号 */
    @Excel(name = "单位编号")
    private Long oId;

    private String oName;

    /** 运动员姓名 */
    @Excel(name = "运动员姓名")
    private String athleteName;

    /** 运动项目名称 */
    @Excel(name = "运动项目名称")
    private String sportName;

    /** 成绩字符串 */
    @Excel(name = "成绩字符串")
    private String score;

    /** 成绩数值 */
    @Excel(name = "成绩数值")
    private Long numberScore;

    /** 排序规则 */
    @Excel(name = "排序规则")
    private String rule;

    /** 破记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "破记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date breakTime;

    public String getoName() {
        return oName;
    }

    public void setoName(String oName) {
        this.oName = oName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setoId(Long oId) 
    {
        this.oId = oId;
    }

    public Long getoId() 
    {
        return oId;
    }
    public void setAthleteName(String athleteName) 
    {
        this.athleteName = athleteName;
    }

    public String getAthleteName() 
    {
        return athleteName;
    }
    public void setSportName(String sportName) 
    {
        this.sportName = sportName;
    }

    public String getSportName() 
    {
        return sportName;
    }
    public void setScore(String score) 
    {
        this.score = score;
    }

    public String getScore() 
    {
        return score;
    }
    public void setNumberScore(Long numberScore) 
    {
        this.numberScore = numberScore;
    }

    public Long getNumberScore() 
    {
        return numberScore;
    }
    public void setRule(String rule) 
    {
        this.rule = rule;
    }

    public String getRule() 
    {
        return rule;
    }
    public void setBreakTime(Date breakTime) 
    {
        this.breakTime = breakTime;
    }

    public Date getBreakTime() 
    {
        return breakTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("oId", getoId())
            .append("athleteName", getAthleteName())
            .append("sportName", getSportName())
            .append("score", getScore())
            .append("numberScore", getNumberScore())
            .append("rule", getRule())
            .append("breakTime", getBreakTime())
            .toString();
    }
}
