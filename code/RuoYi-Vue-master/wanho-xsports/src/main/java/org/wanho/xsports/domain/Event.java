package org.wanho.xsports.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 赛项对象 event
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Event extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 赛项编号 */
    private Long id;

    /** 运动会编号 */
    @Excel(name = "运动会编号")
    private Long gId;

    private String gName;

    private Long value;

    

    /** 赛项名称 */
    @Excel(name = "赛项名称")
    private String name;

    /** 所属分项 */
    @Excel(name = "所属分项")
    private String discipline;

    /** 所属大项 */
    @Excel(name = "所属大项")
    private String sport;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 关键字 */
    @Excel(name = "关键字")
    private String keywords;

    /** 组别 */
    @Excel(name = "组别")
    private String groupings;

    /** 性别 */
    @Excel(name = "性别")
    private String gender;

    /** 报名开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报名开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 报名结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报名结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /*
     * 报名人数
     */
    private Integer num;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setgId(Long gId) 
    {
        this.gId = gId;
    }

    public Long getgId() 
    {
        return gId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDiscipline(String discipline) 
    {
        this.discipline = discipline;
    }

    public String getDiscipline() 
    {
        return discipline;
    }
    public void setSport(String sport) 
    {
        this.sport = sport;
    }

    public String getSport() 
    {
        return sport;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setGroupings(String groupings) 
    {
        this.groupings = groupings;
    }

    public String getGroupings() 
    {
        return groupings;
    }
    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getGender() 
    {
        return gender;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gId", getgId())
            .append("name", getName())
            .append("discipline", getDiscipline())
            .append("sport", getSport())
            .append("remarks", getRemarks())
            .append("keywords", getKeywords())
            .append("groupings", getGroupings())
            .append("gender", getGender())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("status", getStatus())
            .toString();
    }
}
