package org.wanho.xsports.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报名对象 signup
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Signup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 运动员编号 */
    private Long aId;

    private String aName;

    private String eName;

    /** 报名编号 */
    private Long id;

    /** 赛项编号 */
    @Excel(name = "赛项编号")
    private Long eId;

    /** 报名时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报名时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date signupTime;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private Integer status;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date verifyTime;

    public void setaId(Long aId) 
    {
        this.aId = aId;
    }

    public Long getaId() 
    {
        return aId;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void seteId(Long eId) 
    {
        this.eId = eId;
    }

    public Long geteId() 
    {
        return eId;
    }
    public void setSignupTime(Date signupTime) 
    {
        this.signupTime = signupTime;
    }

    public Date getSignupTime() 
    {
        return signupTime;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setVerifyTime(Date verifyTime) 
    {
        this.verifyTime = verifyTime;
    }

    public Date getVerifyTime() 
    {
        return verifyTime;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("aId", getaId())
            .append("id", getId())
            .append("eId", geteId())
            .append("signupTime", getSignupTime())
            .append("status", getStatus())
            .append("verifyTime", getVerifyTime())
            .toString();
    }
}
