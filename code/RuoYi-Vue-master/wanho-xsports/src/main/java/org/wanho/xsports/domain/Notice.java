package org.wanho.xsports.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公告对象 notice
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Notice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公告编号 */
    private Long id;

    /** 赛程编号 */
    @Excel(name = "赛程编号")
    private Long sId;

    private String sName;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 关键词 */
    @Excel(name = "关键词")
    private String keywords;

    /** 文本内容 */
    @Excel(name = "文本内容")
    private String content;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setsId(Long sId) 
    {
        this.sId = sId;
    }

    public Long getsId() 
    {
        return sId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sId", getsId())
            .append("title", getTitle())
            .append("keywords", getKeywords())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("status", getStatus())
            .toString();
    }
}
