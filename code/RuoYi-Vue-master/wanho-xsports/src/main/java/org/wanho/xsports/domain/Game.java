package org.wanho.xsports.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.entity.SysDept;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 运动会对象 game
 * 
 * @author wanho
 * @date 2023-08-24
 */
public class Game extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 运动会编号 */
    private Long id;

    /** 单位编号 */
    @Excel(name = "单位编号")
    private Long oId;

    private String deptName;

    /** 运动会名称 */
    @Excel(name = "运动会名称")
    private String name;

    /** 运动会开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "运动会开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 运动会结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "运动会结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setoId(Long oId) 
    {
        this.oId = oId;
    }

    public Long getoId() 
    {
        return oId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("oId", getoId())
            .append("name", getName())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("remarks", getRemarks())
            .toString();
    }
}
