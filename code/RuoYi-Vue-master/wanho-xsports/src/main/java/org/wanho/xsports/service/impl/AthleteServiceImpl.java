package org.wanho.xsports.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.sign.Md5Utils;
import com.ruoyi.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.mapper.AthleteMapper;
import org.wanho.xsports.domain.Athlete;
import org.wanho.xsports.service.IAthleteService;

/**
 * 运动员Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class AthleteServiceImpl implements IAthleteService 
{
    @Autowired
    private AthleteMapper athleteMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;


    @Override
    public Athlete login(Athlete athlete) throws Exception {
        Athlete query = new Athlete();
        query.setUsername(athlete.getUsername());
        List<Athlete> athletes = athleteMapper.selectAthleteList(query);
        if(athletes.isEmpty() || !athletes.get(0).getPassword()
                .equals(Md5Utils.hash(athlete.getPassword()))){
            throw new Exception("用户名或密码错误");
        }
        return athletes.get(0);
    }

    /**
     * 查询运动员
     * 
     * @param id 运动员主键
     * @return 运动员
     */
    @Override
    public Athlete selectAthleteById(Long id)
    {
        return athleteMapper.selectAthleteById(id);
    }

    /**
     * 查询运动员列表
     * 
     * @param athlete 运动员
     * @return 运动员
     */
    @Override
    public List<Athlete> selectAthleteList(Athlete athlete)
    {
        athlete.setoId(SecurityUtils.getLoginUser().getDeptId());
        List<Athlete> athletes = athleteMapper.selectAthleteList(athlete);
        for(Athlete a : athletes){
            SysDept sysDept = sysDeptMapper.selectDeptById(a.getoId());
            a.setoName(sysDept.getDeptName());
        }
        return athletes;
    }

    /**
     * 新增运动员
     * 
     * @param athlete 运动员
     * @return 结果
     */
    @Override
    public int insertAthlete(Athlete athlete)
    {
        return athleteMapper.insertAthlete(athlete);
    }

    /**
     * 修改运动员
     * 
     * @param athlete 运动员
     * @return 结果
     */
    @Override
    public int updateAthlete(Athlete athlete)
    {
        Athlete a = new Athlete();
        a.setId(athlete.getId());
        a.setBirth(athlete.getBirth());
        a.setGender(athlete.getGender());
        a.setEmail(athlete.getEmail());
        a.setPassword(Md5Utils.hash(athlete.getPassword()));
        a.setStatus(1);
        return athleteMapper.updateAthlete(a);
    }

    /**
     * 批量删除运动员
     * 
     * @param ids 需要删除的运动员主键
     * @return 结果
     */
    @Override
    public int deleteAthleteByIds(Long[] ids)
    {
        return athleteMapper.deleteAthleteByIds(ids);
    }

    /**
     * 删除运动员信息
     * 
     * @param id 运动员主键
     * @return 结果
     */
    @Override
    public int deleteAthleteById(Long id)
    {
        return athleteMapper.deleteAthleteById(id);
    }

    @Override
    public String importUser(List<Athlete> stuList, boolean updateSupport, String operName) {
        for(Athlete a : stuList){
            a.setStatus(0);
            a.setRegisterTime(new Date());
            a.setoId(SecurityUtils.getLoginUser().getDeptId());
            a.setPassword(Md5Utils.hash("123456"));
            athleteMapper.insertAthlete(a);
        }
        return "添加成功";
    }
}
