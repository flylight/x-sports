package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Athlete;

/**
 * 运动员Service接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface IAthleteService 
{
    /**
     * 查询运动员
     * 
     * @param id 运动员主键
     * @return 运动员
     */
    public Athlete selectAthleteById(Long id);

    /**
     * 查询运动员列表
     * 
     * @param athlete 运动员
     * @return 运动员集合
     */
    public List<Athlete> selectAthleteList(Athlete athlete);

    /**
     * 新增运动员
     * 
     * @param athlete 运动员
     * @return 结果
     */
    public int insertAthlete(Athlete athlete);

    /**
     * 修改运动员
     * 
     * @param athlete 运动员
     * @return 结果
     */
    public int updateAthlete(Athlete athlete);

    /**
     * 批量删除运动员
     * 
     * @param ids 需要删除的运动员主键集合
     * @return 结果
     */
    public int deleteAthleteByIds(Long[] ids);

    /**
     * 删除运动员信息
     * 
     * @param id 运动员主键
     * @return 结果
     */
    public int deleteAthleteById(Long id);

    /*
     * 批量导入用户
     */
    String importUser(List<Athlete> stuList, boolean updateSupport, String operName);

    /*
     * 登录功能
     */
    Athlete login(Athlete athlete) throws Exception;
}
