package org.wanho.xsports.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wanho.xsports.mapper.HistoryrecordMapper;
import org.wanho.xsports.domain.Historyrecord;
import org.wanho.xsports.service.IHistoryrecordService;

/**
 * 历史记录Service业务层处理
 * 
 * @author wanho
 * @date 2023-08-24
 */
@Service
public class HistoryrecordServiceImpl implements IHistoryrecordService 
{
    @Autowired
    private HistoryrecordMapper historyrecordMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询历史记录
     * 
     * @param id 历史记录主键
     * @return 历史记录
     */
    @Override
    public Historyrecord selectHistoryrecordById(Long id)
    {
        return historyrecordMapper.selectHistoryrecordById(id);
    }

    /**
     * 查询历史记录列表
     * 
     * @param historyrecord 历史记录
     * @return 历史记录
     */
    @Override
    public List<Historyrecord> selectHistoryrecordList(Historyrecord historyrecord)
    {
        List<Historyrecord> historyrecords = historyrecordMapper.selectHistoryrecordList(historyrecord);
        for(Historyrecord h : historyrecords){
            SysDept dept = sysDeptMapper.selectDeptById(h.getoId());
            h.setoName(dept.getDeptName());
        }
        return historyrecords;
    }

    /**
     * 新增历史记录
     * 
     * @param historyrecord 历史记录
     * @return 结果
     */
    @Override
    public int insertHistoryrecord(Historyrecord historyrecord)
    {
        return historyrecordMapper.insertHistoryrecord(historyrecord);
    }

    /**
     * 修改历史记录
     * 
     * @param historyrecord 历史记录
     * @return 结果
     */
    @Override
    public int updateHistoryrecord(Historyrecord historyrecord)
    {
        return historyrecordMapper.updateHistoryrecord(historyrecord);
    }

    /**
     * 批量删除历史记录
     * 
     * @param ids 需要删除的历史记录主键
     * @return 结果
     */
    @Override
    public int deleteHistoryrecordByIds(Long[] ids)
    {
        return historyrecordMapper.deleteHistoryrecordByIds(ids);
    }

    /**
     * 删除历史记录信息
     * 
     * @param id 历史记录主键
     * @return 结果
     */
    @Override
    public int deleteHistoryrecordById(Long id)
    {
        return historyrecordMapper.deleteHistoryrecordById(id);
    }
}
