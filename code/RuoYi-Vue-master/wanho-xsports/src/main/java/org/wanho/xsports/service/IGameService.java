package org.wanho.xsports.service;

import java.util.List;
import org.wanho.xsports.domain.Game;

/**
 * 运动会Service接口
 * 
 * @author wanho
 * @date 2023-08-24
 */
public interface IGameService 
{
    /**
     * 查询运动会
     * 
     * @param id 运动会主键
     * @return 运动会
     */
    public Game selectGameById(Long id);

    /**
     * 查询运动会列表
     * 
     * @param game 运动会
     * @return 运动会集合
     */
    public List<Game> selectGameList(Game game);

    /**
     * 新增运动会
     * 
     * @param game 运动会
     * @return 结果
     */
    public int insertGame(Game game);

    /**
     * 修改运动会
     * 
     * @param game 运动会
     * @return 结果
     */
    public int updateGame(Game game);

    /**
     * 批量删除运动会
     * 
     * @param ids 需要删除的运动会主键集合
     * @return 结果
     */
    public int deleteGameByIds(Long[] ids);

    /**
     * 删除运动会信息
     * 
     * @param id 运动会主键
     * @return 结果
     */
    public int deleteGameById(Long id);
}
