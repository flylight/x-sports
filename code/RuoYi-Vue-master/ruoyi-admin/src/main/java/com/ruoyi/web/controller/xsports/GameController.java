package com.ruoyi.web.controller.xsports;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import org.wanho.xsports.domain.Game;
import org.wanho.xsports.service.IGameService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运动会Controller
 * 
 * @author wanho
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/xsports/game")
public class GameController extends BaseController
{
    @Autowired
    private IGameService gameService;

    /**
     * 查询运动会列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:game:list')")
    @GetMapping("/list")
    public TableDataInfo list(Game game)
    {
        startPage();
        List<Game> list = gameService.selectGameList(game);
        return getDataTable(list);
    }

    @GetMapping("/all")
    public List<Game> all(Game game){
        return gameService.selectGameList(game);
    }

    /**
     * 导出运动会列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:game:export')")
    @Log(title = "运动会", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Game game)
    {
        List<Game> list = gameService.selectGameList(game);
        ExcelUtil<Game> util = new ExcelUtil<Game>(Game.class);
        util.exportExcel(response, list, "运动会数据");
    }

    /**
     * 获取运动会详细信息
     */
    @PreAuthorize("@ss.hasPermi('xsports:game:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(gameService.selectGameById(id));
    }

    /**
     * 新增运动会
     */
    @PreAuthorize("@ss.hasPermi('xsports:game:add')")
    @Log(title = "运动会", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Game game)
    {
        return toAjax(gameService.insertGame(game));
    }

    /**
     * 修改运动会
     */
    @PreAuthorize("@ss.hasPermi('xsports:game:edit')")
    @Log(title = "运动会", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Game game)
    {
        return toAjax(gameService.updateGame(game));
    }

    /**
     * 删除运动会
     */
    @PreAuthorize("@ss.hasPermi('xsports:game:remove')")
    @Log(title = "运动会", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(gameService.deleteGameByIds(ids));
    }
}
