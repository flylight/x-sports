package com.ruoyi.web.controller.xsports;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import org.wanho.xsports.domain.Signup;
import org.wanho.xsports.service.ISignupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报名Controller
 * 
 * @author wanho
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/xsports/signup")
public class SignupController extends BaseController
{
    @Autowired
    private ISignupService signupService;

    /**
     * 查询报名列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:signup:list')")
    @GetMapping("/list")
    public TableDataInfo list(Signup signup)
    {
        startPage();
        List<Signup> list = signupService.selectSignupList(signup);
        return getDataTable(list);
    }

    @Anonymous
    @GetMapping("/my")
    public List<Signup> my(Signup signup){
        return signupService.selectSignupList(signup);
    }

    /**
     * 导出报名列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:signup:export')")
    @Log(title = "报名", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Signup signup)
    {
        List<Signup> list = signupService.selectSignupList(signup);
        ExcelUtil<Signup> util = new ExcelUtil<Signup>(Signup.class);
        util.exportExcel(response, list, "报名数据");
    }

    /**
     * 获取报名详细信息
     */
    @PreAuthorize("@ss.hasPermi('xsports:signup:query')")
    @GetMapping(value = "/{aId}")
    public AjaxResult getInfo(@PathVariable("aId") Long aId)
    {
        return success(signupService.selectSignupByAId(aId));
    }

    /**
     * 新增报名
     */
    @PreAuthorize("@ss.hasPermi('xsports:signup:add')")
    @Log(title = "报名", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Signup signup)
    {
        return toAjax(signupService.insertSignup(signup));
    }

    @Anonymous
    @GetMapping("/insert")
    public AjaxResult insert(Signup signup){
        return toAjax(signupService.insertSignup(signup));
    }

    @GetMapping("/update")
    public AjaxResult update(Signup signup){
        return toAjax(signupService.updateSignup(signup));
    }

    /**
     * 修改报名
     */
    @PreAuthorize("@ss.hasPermi('xsports:signup:edit')")
    @Log(title = "报名", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Signup signup)
    {
        return toAjax(signupService.updateSignup(signup));
    }

    /**
     * 删除报名
     */
    @PreAuthorize("@ss.hasPermi('xsports:signup:remove')")
    @Log(title = "报名", businessType = BusinessType.DELETE)
	@DeleteMapping("/{aIds}")
    public AjaxResult remove(@PathVariable Long[] aIds)
    {
        return toAjax(signupService.deleteSignupByAIds(aIds));
    }
}
