package com.ruoyi.web.controller.xsports;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import org.wanho.xsports.domain.Event;
import org.wanho.xsports.service.IEventService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 赛项Controller
 * 
 * @author wanho
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/xsports/event")
public class EventController extends BaseController
{
    @Autowired
    private IEventService eventService;

    /**
     * 查询赛项列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:event:list')")
    @GetMapping("/list")
    public TableDataInfo list(Event event)
    {
        startPage();
        List<Event> list = eventService.selectEventList(event);
        return getDataTable(list);
    }

    /**
     * 获取所有的赛项
     */
    @Anonymous
    @GetMapping("/all")
    public List<Event> all(Event event){
        return eventService.selectEventList(event);
    }

    /**
     * 导出赛项列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:event:export')")
    @Log(title = "赛项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Event event)
    {
        List<Event> list = eventService.selectEventList(event);
        ExcelUtil<Event> util = new ExcelUtil<Event>(Event.class);
        util.exportExcel(response, list, "赛项数据");
    }

    /**
     * 获取赛项详细信息
     */
    @PreAuthorize("@ss.hasPermi('xsports:event:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(eventService.selectEventById(id));
    }

    /**
     * 新增赛项
     */
    @PreAuthorize("@ss.hasPermi('xsports:event:add')")
    @Log(title = "赛项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Event event)
    {
        return toAjax(eventService.insertEvent(event));
    }

    /**
     * 修改赛项
     */
    @PreAuthorize("@ss.hasPermi('xsports:event:edit')")
    @Log(title = "赛项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Event event)
    {
        return toAjax(eventService.updateEvent(event));
    }

    /**
     * 删除赛项
     */
    @PreAuthorize("@ss.hasPermi('xsports:event:remove')")
    @Log(title = "赛项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(eventService.deleteEventByIds(ids));
    }
}
