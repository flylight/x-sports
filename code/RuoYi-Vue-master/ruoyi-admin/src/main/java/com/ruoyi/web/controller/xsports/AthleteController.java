package com.ruoyi.web.controller.xsports;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import org.springframework.web.multipart.MultipartFile;
import org.wanho.xsports.domain.Athlete;
import org.wanho.xsports.service.IAthleteService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运动员Controller
 * 
 * @author wanho
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/xsports/athlete")
public class AthleteController extends BaseController
{
    @Autowired
    private IAthleteService athleteService;

    @Log(title = "运动员基本信息", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Athlete> util = new ExcelUtil<>(Athlete.class);
        List<Athlete> stuList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = athleteService.importUser(stuList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 运动员登录功能
     */
    @Anonymous
    @GetMapping("/login")
    public AjaxResult login(Athlete athlete){
        try {
            Athlete a = athleteService.login(athlete);
            return AjaxResult.success(a);
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 运动员完善信息
     * @param athlete
     * @return
     */
    @Anonymous
    @GetMapping("/update")
    public AjaxResult update(Athlete athlete){
        return toAjax(athleteService.updateAthlete(athlete));
    }

    /**
     * 查询运动员列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:athlete:list')")
    @GetMapping("/list")
    public TableDataInfo list(Athlete athlete)
    {
        startPage();
        List<Athlete> list = athleteService.selectAthleteList(athlete);
        return getDataTable(list);
    }

    /**
     * 导出运动员列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:athlete:export')")
    @Log(title = "运动员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Athlete athlete)
    {
        List<Athlete> list = athleteService.selectAthleteList(athlete);
        ExcelUtil<Athlete> util = new ExcelUtil<Athlete>(Athlete.class);
        util.exportExcel(response, list, "运动员数据");
    }

    /**
     * 获取运动员详细信息
     */
    @PreAuthorize("@ss.hasPermi('xsports:athlete:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(athleteService.selectAthleteById(id));
    }

    /**
     * 新增运动员
     */
    @PreAuthorize("@ss.hasPermi('xsports:athlete:add')")
    @Log(title = "运动员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Athlete athlete)
    {
        return toAjax(athleteService.insertAthlete(athlete));
    }

    /**
     * 修改运动员
     */
    @PreAuthorize("@ss.hasPermi('xsports:athlete:edit')")
    @Log(title = "运动员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Athlete athlete)
    {
        return toAjax(athleteService.updateAthlete(athlete));
    }

    /**
     * 删除运动员
     */
    @PreAuthorize("@ss.hasPermi('xsports:athlete:remove')")
    @Log(title = "运动员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(athleteService.deleteAthleteByIds(ids));
    }
}
