package com.ruoyi.web.controller.xsports;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import org.wanho.xsports.domain.Groupings;
import org.wanho.xsports.service.IGroupingsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 组别Controller
 * 
 * @author wanho
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/xsports/groupings")
public class GroupingsController extends BaseController
{
    @Autowired
    private IGroupingsService groupingsService;

    /**
     * 查询组别列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:groupings:list')")
    @GetMapping("/list")
    public TableDataInfo list(Groupings groupings)
    {
        startPage();
        List<Groupings> list = groupingsService.selectGroupingsList(groupings);
        return getDataTable(list);
    }

    @GetMapping("/all")
    public List<Groupings> all(Groupings groupings)
    {
        return groupingsService.selectGroupingsList(groupings);
    }

    /**
     * 导出组别列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:groupings:export')")
    @Log(title = "组别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Groupings groupings)
    {
        List<Groupings> list = groupingsService.selectGroupingsList(groupings);
        ExcelUtil<Groupings> util = new ExcelUtil<Groupings>(Groupings.class);
        util.exportExcel(response, list, "组别数据");
    }

    /**
     * 获取组别详细信息
     */
    @PreAuthorize("@ss.hasPermi('xsports:groupings:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(groupingsService.selectGroupingsById(id));
    }

    /**
     * 新增组别
     */
    @PreAuthorize("@ss.hasPermi('xsports:groupings:add')")
    @Log(title = "组别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Groupings groupings)
    {
        return toAjax(groupingsService.insertGroupings(groupings));
    }

    /**
     * 修改组别
     */
    @PreAuthorize("@ss.hasPermi('xsports:groupings:edit')")
    @Log(title = "组别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Groupings groupings)
    {
        return toAjax(groupingsService.updateGroupings(groupings));
    }

    /**
     * 删除组别
     */
    @PreAuthorize("@ss.hasPermi('xsports:groupings:remove')")
    @Log(title = "组别", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupingsService.deleteGroupingsByIds(ids));
    }
}
