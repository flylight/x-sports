package com.ruoyi.web.controller.xsports;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import org.wanho.xsports.domain.Historyrecord;
import org.wanho.xsports.service.IHistoryrecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 历史记录Controller
 * 
 * @author wanho
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/xsports/historyrecord")
public class HistoryrecordController extends BaseController
{
    @Autowired
    private IHistoryrecordService historyrecordService;

    /**
     * 查询历史记录列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:historyrecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(Historyrecord historyrecord)
    {
        startPage();
        List<Historyrecord> list = historyrecordService.selectHistoryrecordList(historyrecord);
        return getDataTable(list);
    }

    /**
     * 导出历史记录列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:historyrecord:export')")
    @Log(title = "历史记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Historyrecord historyrecord)
    {
        List<Historyrecord> list = historyrecordService.selectHistoryrecordList(historyrecord);
        ExcelUtil<Historyrecord> util = new ExcelUtil<Historyrecord>(Historyrecord.class);
        util.exportExcel(response, list, "历史记录数据");
    }

    /**
     * 获取历史记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('xsports:historyrecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(historyrecordService.selectHistoryrecordById(id));
    }

    /**
     * 新增历史记录
     */
    @PreAuthorize("@ss.hasPermi('xsports:historyrecord:add')")
    @Log(title = "历史记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Historyrecord historyrecord)
    {
        return toAjax(historyrecordService.insertHistoryrecord(historyrecord));
    }

    /**
     * 修改历史记录
     */
    @PreAuthorize("@ss.hasPermi('xsports:historyrecord:edit')")
    @Log(title = "历史记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Historyrecord historyrecord)
    {
        return toAjax(historyrecordService.updateHistoryrecord(historyrecord));
    }

    /**
     * 删除历史记录
     */
    @PreAuthorize("@ss.hasPermi('xsports:historyrecord:remove')")
    @Log(title = "历史记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(historyrecordService.deleteHistoryrecordByIds(ids));
    }
}
