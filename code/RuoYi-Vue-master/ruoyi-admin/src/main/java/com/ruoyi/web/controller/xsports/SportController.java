package com.ruoyi.web.controller.xsports;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import org.wanho.xsports.domain.Sport;
import org.wanho.xsports.service.ISportService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运动项目Controller
 * 
 * @author wanho
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/xsports/sport")
public class SportController extends BaseController
{
    @Autowired
    private ISportService sportService;

    /**
     * 查询运动项目列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:sport:list')")
    @GetMapping("/list")
    public TableDataInfo list(Sport sport)
    {
        startPage();
        List<Sport> list = sportService.selectSportList(sport);
        return getDataTable(list);
    }

    @GetMapping("/allParent")
    public List<Sport> allParent(){
        return sportService.selectParent();
    }

    @GetMapping("/allSmall")
    public List<Sport> allSmall(Integer pId){
        Sport s = new Sport();
        s.setParent(pId);
        return sportService.selectSportList(s);
    }

    /**
     * 导出运动项目列表
     */
    @PreAuthorize("@ss.hasPermi('xsports:sport:export')")
    @Log(title = "运动项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Sport sport)
    {
        List<Sport> list = sportService.selectSportList(sport);
        ExcelUtil<Sport> util = new ExcelUtil<Sport>(Sport.class);
        util.exportExcel(response, list, "运动项目数据");
    }

    /**
     * 获取运动项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('xsports:sport:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(sportService.selectSportById(id));
    }

    /**
     * 新增运动项目
     */
    @PreAuthorize("@ss.hasPermi('xsports:sport:add')")
    @Log(title = "运动项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Sport sport)
    {
        return toAjax(sportService.insertSport(sport));
    }

    /**
     * 修改运动项目
     */
    @PreAuthorize("@ss.hasPermi('xsports:sport:edit')")
    @Log(title = "运动项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Sport sport)
    {
        return toAjax(sportService.updateSport(sport));
    }

    /**
     * 删除运动项目
     */
    @PreAuthorize("@ss.hasPermi('xsports:sport:remove')")
    @Log(title = "运动项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(sportService.deleteSportByIds(ids));
    }
}
